import { Component, OnInit, ViewChildren, ElementRef, ViewChild, } from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { KeyValue } from '../beans/KeyValue';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-chart',
  templateUrl: './product-chart.component.html',
  styleUrls: ['./product-chart.component.css']
})
export class ProductChartComponent {
  @ViewChild('orderamountperdayC') orderamountperdayC: ElementRef;
  @ViewChild('productonstockalertC') productonstockalertC: ElementRef;
  @ViewChild('productsoldperdayC') productsoldperdayC: ElementRef;
  @ViewChild('sellPurchaseDonut') sellPurchaseDonut:ElementRef;
  @ViewChild('orderC') orderC:ElementRef;
  @ViewChild('revenueC') revenueC:ElementRef;
  @ViewChild('avgOrderValueC') avgOrderValueC:ElementRef;
  @ViewChild('uniqueVisitorsC') uniqueVisitorsC:ElementRef;


  data: string[];
  orderChart:Chart
  stockAlertChart:Chart
  sellPurchageChart:Chart
  productSoldChart:Chart
  orderamountperday = 'http://localhost:8080/orderamountperday';
  productonstockalert = 'http://localhost:8080/productonstockalert';
  productsoldperday= 'http://localhost:8080/productsoldperday';
  sellPurchaseCount='http://localhost:8080/sellPurchaseCount';

  barchart = [];
  orderamountperdayChart = [];
  selectRange=null;
  constructor(private http: HttpClient) { }
  ngOnInit() {
    this.getOrderAmountBarChart(1);
    this.getProductSold(1);
    this.getProductsOnStockAlert(10);
    this.getSellPurchase(1);
   

  this.http.get(`${environment.apiURL}/ordercountpermonths`).subscribe((result: KeyValue[]) => {
    let KEYES = [];
    let VALUES = [];
    result.forEach(x => {
      KEYES.push(x.key);
      VALUES.push(x.value);
    });

    let ctx = this.orderC.nativeElement.getContext('2d');
    new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      // The data for our dataset
      data: {
        labels: KEYES,
        datasets: [{
          label: "Sales",
          // processed_json
          // data: [100,200,processed_json[0]],
          data: VALUES,
          backgroundColor: [
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#3a3afb',
            '#8416fe'
          ]
        }]
      },
      // Configuration options go here
      options: {
        legend: {
          display: false
        },
        animation: {
          easing: "easeInOutBack"
        },
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              fontColor: "#cccccc",
              beginAtZero: !0,
              padding: 0
            },
            gridLines: {
              zeroLineColor: "transparent"
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              zeroLineColor: "transparent",
              display: !1
            },
            ticks: {
              beginAtZero: !0,
              padding: 0,
              fontColor: "#cccccc"
            }
          }]
        }
      }
    });
  });

  this.http.get(`${environment.apiURL}/orderamountperday/4`).subscribe((result: KeyValue[]) => {
    let KEYES = [];
    let VALUES = [];
    result.forEach(x => {
      KEYES.push(x.key);
      VALUES.push(x.value);
    });

    let ctx = this.revenueC.nativeElement.getContext('2d');
    new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      // The data for our dataset
      data: {
        labels: KEYES,
        datasets: [{
          label: "Sales",
          // processed_json
          // data: [100,200,processed_json[0]],
          data: VALUES,
          backgroundColor: [
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#3a3afb',
            '#8416fe'
          ]
        }]
      },
      // Configuration options go here
      options: {
        legend: {
          display: false
        },
        animation: {
          easing: "easeInOutBack"
        },
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              fontColor: "#cccccc",
              beginAtZero: !0,
              padding: 0
            },
            gridLines: {
              zeroLineColor: "transparent"
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              zeroLineColor: "transparent",
              display: !1
            },
            ticks: {
              beginAtZero: !0,
              padding: 0,
              fontColor: "#cccccc"
            }
          }]
        }
      }
    });
  });

  this.http.get(`${environment.apiURL}/orderavgamount`).subscribe((result: KeyValue[]) => {
    let KEYES = [];
    let VALUES = [];
    result.forEach(x => {
      KEYES.push(x.key);
      VALUES.push(x.value);
    });

    let ctx = this.avgOrderValueC.nativeElement.getContext('2d');
    new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      // The data for our dataset
      data: {
        labels: KEYES,
        datasets: [{
          label: "Sales",
          // processed_json
          // data: [100,200,processed_json[0]],
          data: VALUES,
          backgroundColor: [
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#3a3afb',
            '#8416fe'
          ]
        }]
      },
      // Configuration options go here
      options: {
        legend: {
          display: false
        },
        animation: {
          easing: "easeInOutBack"
        },
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              fontColor: "#cccccc",
              beginAtZero: !0,
              padding: 0
            },
            gridLines: {
              zeroLineColor: "transparent"
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              zeroLineColor: "transparent",
              display: !1
            },
            ticks: {
              beginAtZero: !0,
              padding: 0,
              fontColor: "#cccccc"
            }
          }]
        }
      }
    });
  });

  this.http.get(`${environment.apiURL}/customercountpermonth`).subscribe((result: KeyValue[]) => {
    let KEYES = [];
    let VALUES = [];
    result.forEach(x => {
      KEYES.push(x.key);
      VALUES.push(x.value);
    });

    let ctx = this.uniqueVisitorsC.nativeElement.getContext('2d');
    new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
      // The data for our dataset
      data: {
        labels: KEYES,
        datasets: [{
          label: "Sales",
          // processed_json
          // data: [100,200,processed_json[0]],
          data: VALUES,
          backgroundColor: [
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#8416fe',
            '#3a3afb',
            '#3a3afb',
            '#8416fe'
          ]
        }]
      },
      // Configuration options go here
      options: {
        legend: {
          display: false
        },
        animation: {
          easing: "easeInOutBack"
        },
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              fontColor: "#cccccc",
              beginAtZero: !0,
              padding: 0
            },
            gridLines: {
              zeroLineColor: "transparent"
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              zeroLineColor: "transparent",
              display: !1
            },
            ticks: {
              beginAtZero: !0,
              padding: 0,
              fontColor: "#cccccc"
            }
          }]
        }
      }
    });
  });

  }

  changeItemStatus(event) {
    let selectedValue=event.target.value;
    selectedValue
    this.getProductsOnStockAlert(selectedValue);
  }

  getOrderAmountBarChart(day) {
    this.http.get(`${this.orderamountperday}/${day}`).subscribe((result: KeyValue[]) => {
      let KEYES = [];
      let VALUES = [];
      result.forEach(x => {
        KEYES.push(x.key);
        VALUES.push(x.value);
      });
      if(this.orderChart) {
        this.orderChart.destroy();
      }
      let ctx = this.orderamountperdayC.nativeElement.getContext('2d');
     this.orderChart=new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',
        // The data for our dataset
        data: {
          labels: KEYES,
          datasets: [{
            label: "Sales",
            // processed_json
            // data: [100,200,processed_json[0]],
            data: VALUES,
            backgroundColor: [
              '#8416fe',
              '#3a3afb',
              '#8416fe',
              '#3a3afb',
              '#8416fe',
              '#3a3afb',
              '#8416fe',
              '#3a3afb',
              '#3a3afb',
              '#8416fe'
            ]
          }]
        },
        // Configuration options go here
        options: {
          legend: {
            display: false
          },
          animation: {
            easing: "easeInOutBack"
          },
          scales: {
            yAxes: [{
              display: true,
              ticks: {
                fontColor: "#cccccc",
                beginAtZero: !0,
                padding: 0
              },
              gridLines: {
                zeroLineColor: "transparent"
              }
            }],
            xAxes: [{
              display: false,
              gridLines: {
                zeroLineColor: "transparent",
                display: !1
              },
              ticks: {
                beginAtZero: !0,
                padding: 0,
                fontColor: "#cccccc"
              }
            }]
          }
        }
      });
    });
  }

  getProductSold(day) {
    this.http.get(`${this.productsoldperday}/${day}`).subscribe((result: KeyValue[]) => {
      let KEYES = [];
      let VALUES = [];
      result.forEach(x => {
        KEYES.push(x.key);
        VALUES.push(x.value);
      });
      if(this.productSoldChart){
        this.productSoldChart.destroy();
      }
      let ctx2 = this.productsoldperdayC.nativeElement.getContext('2d');
      this.productSoldChart= new Chart(ctx2, {
        type: 'bar',
        data: {
          labels: KEYES,
          datasets: [
            {
              data: VALUES,
              // borderColor: '#3cba9f',
              backgroundColor: [
                '#8416fe',
                '#3a3afb',
                '#8416fe',
                '#3a3afb',
                '#8416fe',
                '#3a3afb',
                '#8416fe',
                '#3a3afb',
                '#3a3afb',
                '#8416fe'
              ],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: false
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    });
  }

  getProductsOnStockAlert(count) {
    this.http.get(`${this.productonstockalert}/${count}`).subscribe((result: KeyValue[]) => {
      let KEYES = [];
      let VALUES = [];
      result.forEach(x => {
        KEYES.push(x.key);
        VALUES.push(x.value);
      });
      if(this.stockAlertChart){
        this.stockAlertChart.destroy();
      }
      let ctx1 = this.productonstockalertC.nativeElement.getContext('2d');
     this.stockAlertChart=  new Chart(ctx1, {
        type: 'bar',
        data: {
          labels: KEYES,
          datasets: [
            {
              data: VALUES,
              borderColor: '#3cba9f',
              backgroundColor: [
                '#8416fe',
                '#3a3afb',
                '#8416fe',
                '#3a3afb',
                '#8416fe',
                '#3a3afb',
                '#8416fe',
                '#3a3afb',
                '#3a3afb',
                '#8416fe'
              ],
              fill: true
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: false
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    });
  }

  getSellPurchase(day) {
    this.http.get(`${this.sellPurchaseCount}/${day}`).subscribe((result: KeyValue[]) => {
      let KEYES = [];
      let VALUES = [];
      result.forEach(x => {
        KEYES.push((x.key==="0"?"Sell":"Purchase"));
        VALUES.push(x.value);
      });
    let ctx3 = this.sellPurchaseDonut.nativeElement.getContext('2d');
    if(this.sellPurchageChart){
      this.sellPurchageChart.destroy();
    }
    this.sellPurchageChart = new Chart(ctx3, {
        type: 'doughnut',
        data: {
            labels: KEYES,
            datasets: [{
                backgroundColor: [
                    "#8919FE",
                    "#12C498",
                    "#F8CB3F",
                    "#E36D68"
                ],
                borderColor: '#fff',
                data: VALUES,
            }]
        },
        // Configuration options go here
        options: {
            legend: {
                display: true
            },
            animation: {
                easing: "easeInOutBack"
            }
        }
    });
  });
  }
}
