import { Component, OnInit } from '@angular/core';
import { Vendor } from '../beans/Vendor';
import { VendorService } from '../services/VendorService';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {


  vendor: Vendor;
  constructor(private vendorService: VendorService, private router: Router, private toastr: ToastrService, private currentRoute: ActivatedRoute) { }

  ngOnInit() {
    let customerId = this.currentRoute.snapshot.paramMap.get('id');
    if (customerId == null) {
      this.vendor = {
        id: 0,
        name: null,
        contactNumber: null,
        emailId: null,
        address: null,
        isActive: 1,
        dateOfBirth: new Date(),
        type: 'Vendor',
        panCardNumber: '',
        bankAccountNumber: '',
        bankAccountName: '',
        ifscCode: ''

      }
    } else {
      this.vendorService.getVendorById(parseInt(customerId)).then(res => {
        this.vendor = res;
      });
    }
  }
  public onDate(event): void {
    this.vendor.dateOfBirth = event.value;
    console.log(event);
  }

  saveCustomer() {
    console.log(this.vendor);
    this.vendorService.saveVendor(this.vendor).then(res => {
      if (res) {
        this.router.navigate(['/home/vendors']);
        this.toastr.success('Submitted Successfully', 'vendor Details');
      } else {
        this.toastr.success('Please check Submitted ', 'vendor Details');
      }
    });
  }

}
