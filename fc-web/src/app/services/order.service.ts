import { Injectable } from '@angular/core';
import { Order } from '../beans/order';
import { OrderItem } from '../beans/order-item';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  formData: Order;
  orderItems: OrderItem[];
  private headers = new Headers({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) { }

  saveOrUpdateOrder() {
    var body = {
      ...this.formData,
      OrderItems: this.orderItems
    };
    return this.http.post(environment.apiURL + '/Order', body);
  }
  saveOrUpdateOrderNew() {
    console.log("Method is called####");
    this.formData.orderItems = this.orderItems;
    return this.http.post(environment.apiURL + '/Order', this.formData);
  }

  getOrderList(type:number) {
    return this.http.get<Order[]>(environment.apiURL + 'order/list/'+type).toPromise();
  }

  getOrderByID(id: number) {
    return this.http.get<Order>(environment.apiURL + '/Order/' + id).toPromise();
  }

  deleteOrder(id: number) {
    return this.http.delete(environment.apiURL + 'Order/' + id).toPromise();
  }

  getItemListByOrderID(orderId: number) {
    return this.http.get<OrderItem[]>(environment.apiURL + '/TodaysOrderList/' + orderId).toPromise();
  }
}
