import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Feedback } from '../beans/Feedback';
import { Customer } from '../beans/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  feedbackServiceURL = "feedbackForm"
  constructor(private http: HttpClient) { }

  saveCustomer(customer: Customer) {
    return this.http.post(environment.apiURL + '/customer', customer).toPromise();
  }

  getCustomers() {
    return this.http.get<Customer[]>(environment.apiURL + '/customers').toPromise();
  }

  deleteCustomer(id:number)
  {
    return this.http.delete(environment.apiURL+'/deletecustomer/'+id).toPromise();
  }

  getCustomerById(id:number)
  {
    return this.http.get<Customer>(environment.apiURL+'/customer/'+id).toPromise();
  }

  storeFeedback(feedbackForm: Feedback) {
    return this.http.post(environment.feedbackServiceURL, feedbackForm).toPromise();
  }
  getUserList() {
    return this.http.get(environment.apiURL + '/user').toPromise();
  }

  getFeedBacks()
  {
    return this.http.get<any[]>(environment.apiURL+'/feedback/list').toPromise();
  }
}
