import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaysOfferReportComponent } from './todays-offer-report.component';

describe('TodaysOfferReportComponent', () => {
  let component: TodaysOfferReportComponent;
  let fixture: ComponentFixture<TodaysOfferReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaysOfferReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaysOfferReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
