import { Component, OnInit } from '@angular/core';
import { ReportService } from '../report.service';

@Component({
  selector: 'app-todays-offer-report',
  templateUrl: './todays-offer-report.component.html',
  styleUrls: ['./todays-offer-report.component.css']
})
export class TodaysOfferReportComponent implements OnInit {

  orderList:any;
  reportId:number=1;

  constructor(private reportService:ReportService) { }

  ngOnInit() {
    this.reportService.reporTodaysOrderList(this.reportId).then(res=>{
      this.orderList=res["data"];
    })
  }

}
