import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CampaignRule } from '../beans/CampaignRule';
import { HttpClient } from '@angular/common/http';
import { Campaign } from '../beans/Campaign';

@Injectable({
  providedIn: 'root'
})
export class CampaignServiceService {

  constructor(private http:HttpClient) { }

  public getAllCampaignRule()
  {
    return this.http.get<CampaignRule[]>(environment.apiURL + '/campaignrule').toPromise();
  }
  public saveCampaign(campaign:Campaign)
  {
    return this.http.post<boolean>(environment.apiURL+'campaign',campaign).toPromise();
  }

  public getCampaignById(campaignId:number)
  {
    return this.http.get<Campaign>(environment.apiURL+'/campaign/'+campaignId).toPromise();
  }

  public getAllCampaigns()
  {
    return this.http.get<Campaign[]>(environment.apiURL+'/campaigns').toPromise();
  }
  public deleteCampaign(id:number)
  {
    return this.http.delete(environment.apiURL+'deletecampaign/'+id).toPromise();
  }

  public lunchCampaign(id:number)
  {
    return this.http.get<Boolean>(environment.apiURL+'/lunchcampaign/'+id).toPromise();
  }
}
