import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Product } from '../beans/Product';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Company } from '../beans/Company';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  private serverUrl ='http://localhost:8080/product'

  private serverBaseUrl='http://localhost:8080/'

  private serverUrlForCompay='http://localhost:8080/company'

  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Product[]>(this.serverUrl);
    }

    getAllProduct() {
        return this.http.get<Product[]>(this.serverUrl).toPromise();
    }
    

    getById(id: number) {
        return this.http.get<Product>(this.serverUrl +"/" +id).toPromise();
    }

    register(prodcut: Product) {
      console.log("calling service")
        return this.http.post<any>(this.serverUrl, prodcut).pipe(catchError(this.errorHandler));
    }
    errorHandler(error:HttpErrorResponse)
    {
        return throwError(error);
    }

    update(product: Product) {
        return this.http.put(this.serverUrl + product.id, product);
    }

    delete(id: number) {
        return this.http.delete(this.serverBaseUrl +'deleteproduct/'+ id).toPromise();
    }
    getAllCompanies()
    {
        return this.http.get<Company[]>(this.serverUrlForCompay);
    }

}
