import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from "@angular/core";


@Injectable()

export class FileService {

    constructor(private _http:HttpClient){}

    downloadFile(file:String){
        var body = {filename:file};
        //debugger;
        return this._http.post('http://localhost:8080/downloadFile/'+'file',body,{
            responseType : 'blob',
            headers:new HttpHeaders().append('Content-Type','application/json')
        });
    }
    
}
