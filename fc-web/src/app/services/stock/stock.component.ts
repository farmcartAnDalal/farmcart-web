import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Customer } from 'src/app/beans/customer';
import { OrderService } from '../order.service';
import { CustomerService } from '../customer.service';
import { OrderItemComponent } from 'src/app/order-item/order-item.component';
import { CustomerComponent } from 'src/app/customer/customer.component';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  customerList: Customer[];
  isValid: boolean = true;

  constructor(private service: OrderService,
    private dialog: MatDialog,
    private customerService: CustomerService,
    private toastr: ToastrService,
    private router: Router,
    private currentRoute: ActivatedRoute) { }

  ngOnInit() {
    let orderID = this.currentRoute.snapshot.paramMap.get('id');
    if (orderID == null)
      this.resetForm();
    else {
      this.resetForm();
      this.service.getOrderByID(parseInt(orderID)).then(res => {
       // debugger
       this.service.formData = res;
        this.service.orderItems = res.orderItems;
        console.log(res)
      });
    }

    //this.customerService.getCustomerList().then(res => this.customerList = res as Customer[]);
    this.customerService.getCustomers().then(res=>this.customerList=res as Customer[]);
  }

  resetForm(form?: NgForm) {
    if (form = null)
      form.resetForm();
    this.service.formData = {
      orderID: null,
      orderNo: Math.floor(100000 + Math.random() * 900000).toString(),
      customerID: 2,
      pMethod: '',
      gTotal: 0,
      deletedOrderItemIDs: '',
      orderItems:[],
      isActive:1,
      orderType:1,
      customerName:'',
      orderDate:new Date(),
      amountPaid:0,
      totalTax:0,
      totalDiscount:0,
      extraCharge:0,
      balanceAmount:0
    };
    this.service.orderItems = [];
  }

  AddOrEditOrderItem(orderItemIndex, OrderID) {
    //debugger
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
    dialogConfig.data = { orderItemIndex, OrderID };
    this.dialog.open(OrderItemComponent, dialogConfig).afterClosed().subscribe(res => {
      this.updateGrandTotal();
    });
  }

  onCreateNewCustomer()
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
    this.dialog.open(CustomerComponent, dialogConfig).afterClosed().subscribe(res => {
      //this.updateGrandTotal();
    });
  }
  refreshCustomer()
  {

  }
  onDeleteOrderItem(orderItemID: number, i: number) {
    if (orderItemID != null)
      this.service.formData.deletedOrderItemIDs += orderItemID + ",";
    this.service.orderItems.splice(i, 1);
    this.updateGrandTotal();
  }

  updateGrandTotal() {
    this.service.formData.gTotal = this.service.orderItems.reduce((prev, curr) => {
      return prev + curr.Total;
    }, 0);
    this.service.formData.totalTax = this.service.orderItems.reduce((prev, curr) => {
      return (parseFloat(prev.toString())  + parseFloat( curr.Tax.toString()));
    }, 0);
    this.service.formData.totalDiscount = this.service.orderItems.reduce((prev, curr) => {
      return (parseFloat(prev.toString())  + parseFloat( curr.Discount.toString()));
    }, 0);
    this.service.formData.gTotal = parseFloat(this.service.formData.gTotal.toFixed(2));
    this.service.formData.totalTax = parseFloat(this.service.formData.totalTax.toFixed(2));
    this.service.formData.totalDiscount = parseFloat(this.service.formData.totalDiscount.toFixed(2));
    this.service.formData.gTotal=this.service.formData.gTotal+this.service.formData.extraCharge;
    this.service.formData.balanceAmount=(this.service.formData.gTotal )-(this.service.formData.amountPaid);
  }

  validateForm() {
    this.isValid = true;
    if (this.service.formData.customerID == 0)
      this.isValid = false;
    else if (this.service.orderItems.length == 0)
      this.isValid = false;
    return this.isValid;
  }


  onSubmit(form: NgForm) {
    console.log("Showing validation===>"+this.validateForm());
    if (this.validateForm()) {
      console.log("Customer Id==>"+ this.service.formData.customerID);
      console.log( this.service.formData ) ;
      this.service.saveOrUpdateOrderNew().subscribe(res => {
        let result = res;
        this.resetForm();
        this.toastr.success('Submitted Successfully');
        this.router.navigate(['home/order/invoice/' +result["orderID"]]);
      })
    }
  }
}
