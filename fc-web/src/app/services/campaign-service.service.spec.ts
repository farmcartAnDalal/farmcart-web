import { TestBed } from '@angular/core/testing';

import { CampaignServiceService } from './campaign-service.service';

describe('CampaignServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CampaignServiceService = TestBed.get(CampaignServiceService);
    expect(service).toBeTruthy();
  });
});
