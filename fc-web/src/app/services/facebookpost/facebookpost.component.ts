import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ReputationService } from '../reputation.service';
import { Comment } from 'src/app/beans/Comment';

@Component({
  selector: 'app-facebookpost',
  templateUrl: './facebookpost.component.html',
  styleUrls: ['./facebookpost.component.css']
})
export class FacebookpostComponent implements OnInit {

  commentList:Comment[]
  constructor(private router: Router, private toastr: ToastrService, private currentRoute: ActivatedRoute ,private repuationService:ReputationService) { }

  ngOnInit() {

    let postId = this.currentRoute.snapshot.paramMap.get('id');
    console.log('Post Id===>'+postId);
    this.repuationService.getCommentsForPost(postId).then(res=>{
      this.commentList=res;
    });
;  }

}
