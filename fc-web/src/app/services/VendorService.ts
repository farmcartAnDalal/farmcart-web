import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Vendor } from '../beans/Vendor';

@Injectable({
  providedIn: 'root'
})
export class VendorService {

  constructor(private http: HttpClient) { }

  saveVendor(Vendor: Vendor) {
    return this.http.post(environment.apiURL + '/vendor', Vendor).toPromise();
  }

  getVendors() {
    return this.http.get<Vendor[]>(environment.apiURL + '/vendors').toPromise();
  }

  deleteVendor(id:number)
  {
    return this.http.delete(environment.apiURL+'/deletevendor/'+id).toPromise();
  }

  getVendorById(id:number)
  {
    return this.http.get<Vendor>(environment.apiURL+'/vendor/'+id).toPromise();
  }
}
