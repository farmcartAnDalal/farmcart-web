import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {


  constructor(private http: HttpClient) { }
  reportList() {
    return this.http.get(environment.apiURL+"reportlist").toPromise();
  }
  reporTodaysOrderList(reportId:number)
  {
    return this.http.get(environment.apiURL+"todaysorderreport/"+reportId).toPromise();
  }

  perdaycashflow()
  {
    return this.http.get<number[]>(environment.apiURL+"perdaycashflow").toPromise();
  }
  getMembers()
  {
    return this.http.get<any>(environment.apiURL+"teammembers").toPromise();
  }

  saveMember(member:any)
  {
    return this.http.post(environment.apiURL+"teammember",member).toPromise();
  }
}
