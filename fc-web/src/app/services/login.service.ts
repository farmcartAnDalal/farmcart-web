import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {User} from '../beans/User'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private serverUrl ='http://localhost:8080/user'

  private loginUrl='http://localhost:8080/login'

  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(this.serverUrl);
    }

    getById(id: number) {
        return this.http.get(this.serverUrl + id);
    }

    register(user: User) {
      console.log("calling service")
      console.log(user);
        return this.http.post<any>(this.serverUrl, user);
    }

    update(user: User) {
        return this.http.put(this.serverUrl + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(this.serverUrl + id);
    }

    login(userName:String,password:String)
    {
      let  param='userName='+userName+'&password='+password;
        return this.http.get<boolean>(this.loginUrl+param);
    }
    loginWithUser(user:User)
    {
        return this.http.post<Boolean>(this.loginUrl,user);
    }
}
