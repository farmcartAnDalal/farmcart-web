import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Post } from '../beans/Post';
import { Comment } from '../beans/Comment';

@Injectable({
  providedIn: 'root'
})
export class ReputationService {

  constructor(private http: HttpClient) { }
  connectFacebook() {
    return this.http.get(environment.faceBookServerURL+"connectFacebook").toPromise();
  }
  getFacebookPost()
  {
    return this.http.get<Post[]>(environment.faceBookServerURL+"facebookposts").toPromise();
  }

  getCommentsForPost(postId:String)
  {
    return this.http.get<Comment[]>(environment.faceBookServerURL+"/getCommentsForPostId/"+postId).toPromise();
  }
  
}
