import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.css']
})
export class PageTitleComponent implements OnInit {

  userProfileClick = false;
  currentUser=null;

  constructor( private router: Router, private route: ActivatedRoute,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.currentUser=this.authenticationService.userName;
  }
  UPtoggle() {

    this.userProfileClick = !this.userProfileClick;
  }

  logOut() {
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

}
