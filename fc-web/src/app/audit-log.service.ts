import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuditLog } from './beans/AuditLog';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuditLogService {

  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<AuditLog[]>(`${environment.apiURL}getaudits`);
    }
}
