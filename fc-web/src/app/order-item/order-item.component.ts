import { Component, OnInit, Inject } from '@angular/core';
import { OrderItem } from '../beans/order-item';
import { Item } from '../beans/item';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrderService } from '../services/order.service';
import { NgForm } from '@angular/forms';
import { ItemService } from '../services/item.service';
import { Product } from '../beans/Product';
import { ProductServiceService } from '../services/product-service.service';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.css']
})
export class OrderItemComponent implements OnInit {

 
  formData: OrderItem;
  productList:Product[];
  isValid: boolean = true;
  stateTax:number;
  govtTax:number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<OrderItemComponent>,
    private orderSevice: OrderService,
    private productServiceService:ProductServiceService) { }

  ngOnInit() {
   // this.itemService.getItemList().then(res => this.itemList = res as Item[]);
    this.productServiceService.getAllProduct().then(res=>this.productList=res as Product[]) ;
    if (this.data.orderItemIndex == null)
      this.formData = {
        OrderItemID: null,
        OrderID: this.data.OrderID,
        ItemID: 0,
        ItemName: '',
        Price: 0,
        Quantity: 0,
        Total: 0,
        Tax:0,
        Discount:0
      }
    else
      this.formData = Object.assign({}, this.orderSevice.orderItems[this.data.orderItemIndex]);
  }

  updateProductPrice(ctrl) {
    console.log("selected index===>"+ctrl);
    if (ctrl.selectedIndex == 0) {
      this.formData.Price = 0;
      this.formData.ItemName = '';
    }
    else {
      this.formData.Price = this.productList[ctrl.selectedIndex - 1].sellPrice;
      this.formData.ItemName = ''+this.productList[ctrl.selectedIndex - 1].name;
      this.stateTax=this.productList[ctrl.selectedIndex - 1].stateGST;
      this.govtTax=this.productList[ctrl.selectedIndex - 1].govtGST;
    }
    this.updateTotal();
  }

  updateTotal() {
    //this.formData.Total = parseFloat((this.formData.Quantity * this.formData.Price).toFixed(2));
    this.formData.Tax=parseFloat((this.formData.Quantity * this.formData.Price * this.govtTax * this.stateTax / 10000).toFixed(2));
    let total = parseFloat((this.formData.Quantity * this.formData.Price).toFixed(2));
    this.formData.Total=total+this.formData.Tax-this.formData.Discount;
  }

  onSubmit(form: NgForm) {
    if (this.validateForm(form.value)) {
      if (this.data.orderItemIndex == null)
        this.orderSevice.orderItems.push(form.value);
      else
        this.orderSevice.orderItems[this.data.orderItemIndex] = form.value;
      this.dialogRef.close();
    }
  }

  validateForm(formData: OrderItem) {
    this.isValid = true;
    if (formData.ItemID == 0)
      this.isValid = false;
    else if (formData.Quantity == 0)
      this.isValid = false;
    return this.isValid;
  }

}
