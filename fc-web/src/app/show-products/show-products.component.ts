import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../services/product-service.service';
import { Product } from '../beans/Product';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-products',
  templateUrl: './show-products.component.html',
  styleUrls: ['./show-products.component.css']
})
export class ShowProductsComponent implements OnInit {

  prods: Product[];
  searchText = '';
  
  constructor(private productService: ProductServiceService,private toastr: ToastrService,private router: Router) { }

  ngOnInit() {
    //this.prods=this.products.getAll() ;
    this.productService.getAll().subscribe(res => {
      this.prods = res;
   });
  }
  refreshList()
  {
    this.productService.getAll().subscribe(res => {
      this.prods = res;
   });
  }

  onCustomerEdit(id:number,name:String)
  {
      console.log("id===>",id,"name==>",name);
      this.router.navigate(['home/productAdd/edit/'+id]);
  }

  onProductDelete(id: number) {
    if (confirm('Are you sure to delete this record?')) {
      this.productService.delete(id).then(res => {
        this.refreshList();
        this.toastr.warning("Deleted Successfully", "Restaurent App.");
      });
    }
  }

}
