import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  status: boolean = false;
  emailDrop: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  collapseClick() {
    this.status = !this.status;
  }

  mailClick() {
    this.emailDrop= !this.emailDrop;
  }


}
