import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { User } from '../beans/User';
import { Http, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services';
import { first } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  loginForm: FormGroup;

  constructor(public logservice1: LoginService
    , private router: Router, private route: ActivatedRoute,
    private authenticationService: AuthenticationService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

  }
  get user() { return this.loginForm.controls; }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    let emailId=this.user.email.value;
    let pass=this.user.password.value;
    this.authenticationService.login(emailId, pass)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/home"]);
        },
        error => {
          console.log(error);
        });

  }

}
