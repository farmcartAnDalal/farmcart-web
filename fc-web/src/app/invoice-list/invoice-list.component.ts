import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Order } from '../beans/order';
import JsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.css']
})
export class InvoiceListComponent implements OnInit {

  orderList: Order[];
  searchText = '';

  constructor(private service: OrderService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    this.service.getOrderList(parseInt("3")).then(res => this.orderList = res as Order[]);
  }

  openInvoice(orderID: number) {
    this.router.navigate(['home/order/invoice/' + orderID]);
  }
  public captureScreen() {
    var data = document.getElementById('inovoiceDownload');
    html2canvas(data).then(canvas => {
      
      var imgWidth = 208;
      var imgHeight = canvas.height * imgWidth / canvas.width;

      const contentDataURL = canvas.toDataURL('image/png')
      var pdf = new JsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF
    });
  }


}
