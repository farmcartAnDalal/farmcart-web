export const USER_TYPES = [
    {id:1,name: 'Admin', displayName: 'Admin'},
    {id:2,name: 'User', displayName: 'User'},
    {id:3,name: 'Seller', displayName: 'Seller'},
    {id:4,name: 'Buyer', displayName: 'Buyer'}
 ]; 

