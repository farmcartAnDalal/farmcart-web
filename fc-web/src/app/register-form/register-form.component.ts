import { Component, OnInit } from '@angular/core';
import { User } from '../beans/User';
import { LoginService } from '../services/login.service';
import { AddressComponent } from '../address/address.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Address } from '../beans/Address';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  userForm: FormGroup;

  userTypes = [
    { id: 1, name: 'Admin', displayName: 'Admin' },
  { id: 2, name: 'User', displayName: 'User' },
  { id: 3, name: 'Seller', displayName: 'Seller' },
  { id: 4, name: 'Buyer', displayName: 'Buyer' }
]

  user = new User();
  dateOfBirth = new Date();
  public addressComp: AddressComponent;
  constructor(public logservice1: LoginService, private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.createUserForm();
  }

  createUserForm() {
    this.userForm = this.formBuilder.group({
      userName: ['Dadasaheb'],
      password: [],
      firstName: [],
      lastName: [],
      userType: [1],
      mobileNumber: [],
      createdAt: [new Date()],
      address: this.formBuilder.group(
        {
          addressLine1: ['flat No-5 Mkc'],
          addressLine2: [],
          city: [],
          state: [],
          country: [],
          pinCode: []
        }
      )
    })


  }
  onSubmit() {
    console.log("Register form request has been sent");
    this.user=this.userForm.value;
    console.log(this.user);
    this.logservice1.register(this.user).subscribe(data=>console.log("success"),error=>console.error("Error!"));
    //this.logservice1.register(this.user).subscribe(res:Response=>{console.log(res)});
    //user :User = []
    //this.user=res

  }



}
