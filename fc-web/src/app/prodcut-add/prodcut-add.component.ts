import { Component, OnInit } from '@angular/core';
import { Product } from '../beans/Product';
import { ProductServiceService } from '../services/product-service.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Company } from '../beans/Company';
import { Address } from '../beans/Address';
import { Router, ActivatedRoute } from '@angular/router';
import { Vendor } from '../beans/Vendor';
import { VendorService } from '../services/VendorService';


@Component({
  selector: 'app-prodcut-add',
  templateUrl: './prodcut-add.component.html',
  styleUrls: ['./prodcut-add.component.css']
})
export class ProdcutAddComponent implements OnInit {

  vendors: Vendor[];
  unitTypes= [{
    id:1,
    name:"Kilo"
  },
  {
    id:2,
    name:"Litre"
  },
  {
    id:3,
    name:"Meter"
  },
  {
    id:4,
    name:"Nug"
  }];
  product: Product;
  constructor(public productservice: ProductServiceService,public vendorService: VendorService, private formBuilder: FormBuilder, private router: Router, private currentRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.getCompanies();
    this.createProductForm();
  }
  createProductForm() {
    let customerId = this.currentRoute.snapshot.paramMap.get('id');
    if (customerId != null) {
      this.productservice.getById(parseInt(customerId)).then(res => {
        this.product = res;
      })
    }
    this.product={
      id:null,
      buyPrice:null,
      expiredate:null,
      govtGST:null,
      name:null,
      quantity:0,
      sellPrice:null,
      stateGST:0,
      uniqueNumber:null,
      unitId:null,
      stockAlert:true,
      description:"",
      vendorId:null
    }
  }

  onSubmit() {

    console.log("Service is called");
    console.log(this.product);
    this.productservice.register(this.product).subscribe(data => {
      console.log(data);
      this.router.navigate(['/home/showProducts']);
    }
      , error => console.log(error.statusText));
  }
  getCompanies() {
    console.log("Service called");
    this.vendorService.getVendors().then(res => {
      this.vendors = res;
    });
  }

  changeInputType(ctrl) { }

}
