import { Component, OnInit, Inject } from '@angular/core';
import { OrderItem } from '../beans/order-item';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-show-items-for-single-order',
  templateUrl: './show-items-for-single-order.component.html',
  styleUrls: ['./show-items-for-single-order.component.css']
})
export class ShowItemsForSingleOrderComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<ShowItemsForSingleOrderComponent>,
  ) { }
  orderItems:OrderItem[]

  ngOnInit() {

    this.orderItems= this.data.res;
  }

}
