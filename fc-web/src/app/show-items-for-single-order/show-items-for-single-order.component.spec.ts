import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowItemsForSingleOrderComponent } from './show-items-for-single-order.component';

describe('ShowItemsForSingleOrderComponent', () => {
  let component: ShowItemsForSingleOrderComponent;
  let fixture: ComponentFixture<ShowItemsForSingleOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowItemsForSingleOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowItemsForSingleOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
