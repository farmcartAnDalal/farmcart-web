import { Component, OnInit } from '@angular/core';
import { ReportService } from '../services/report.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  reportList:any
  selectedReportId:number=1;
  constructor(private reportService:ReportService,private router: Router) { }

  ngOnInit() {
    this.reportService.reportList().then(res=>{
      this.reportList=res;
    })
  }

  loadReport()
  {
    if(this.selectedReportId==1)
    {
      this.router.navigate(['/home/report/todaysorderreport']);
    }
  }

}
