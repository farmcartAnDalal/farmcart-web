import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Customer } from 'src/app/beans/customer';
import { CustomerService } from 'src/app/services/customer.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-customer-popup',
  templateUrl: './customer.component.html'
})
export class CustomerComponentPopUp implements OnInit {

  customer:Customer;
  constructor(private customerService :CustomerService, private router: Router, 
    public dialogRef: MatDialogRef<CustomerComponentPopUp>,private toastr: ToastrService, private currentRoute: ActivatedRoute) { }

  ngOnInit() {
    let customerId = this.currentRoute.snapshot.paramMap.get('id');
    if(customerId==null){
      this.customer={
        id:0,
        name:null,
        contactNumber:null,
        emailId:null,
        address:null,
        isActive:1,
        dateOfBirth:new Date(),
        type:'Customer'
      }
    }else
    {
      this.customerService.getCustomerById(parseInt(customerId)).then(res=>{
        this.customer=res;
      });
    }
  }
  public onDate(event): void {
    this.customer.dateOfBirth = event.value;
    console.log(event);
  }

  saveCustomer()
  {
    console.log(this.customer);
    this.customerService.saveCustomer(this.customer).then(res=>{
      if(res){
        this.dialogRef.close();
        this.toastr.success('Submitted Successfully', 'Customer Details');
      }else{
        this.toastr.success('Please check Submitted ', 'Customer Details');
      }
    });
  }
}
