import { Component, OnInit } from '@angular/core';
import { Customer } from '../beans/customer';
import { CustomerService } from '../services/customer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import html2canvas from 'html2canvas';
import JsPDF from 'jspdf';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  customer:Customer;
  constructor(private customerService :CustomerService, private router: Router, private toastr: ToastrService, private currentRoute: ActivatedRoute) { }

  ngOnInit() {
    let customerId = this.currentRoute.snapshot.paramMap.get('id');
    if(customerId==null){
      this.customer={
        id:0,
        name:null,
        contactNumber:null,
        emailId:null,
        address:null,
        isActive:1,
        dateOfBirth:new Date(),
        type:'Customer'
      }
    }else
    {
      this.customerService.getCustomerById(parseInt(customerId)).then(res=>{
        this.customer=res;
      });
    }
  }
  public onDate(event): void {
    this.customer.dateOfBirth = event.value;
    console.log(event);
  }

  saveCustomer()
  {
    console.log(this.customer);
    this.customerService.saveCustomer(this.customer).then(res=>{
      if(res){
        this.router.navigate(['/home/customers']);
        this.toastr.success('Submitted Successfully', 'Customer Details');
      }else{
        this.toastr.success('Please check Submitted ', 'Customer Details');
      }
    });
  }

}
