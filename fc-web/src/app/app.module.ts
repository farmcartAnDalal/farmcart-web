import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { platformBrowserDynamic }  from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule ,routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import {LoginService} from './services/login.service';
import { HomeComponent } from './home/home.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { ReportAreaComponent } from './report-area/report-area.component';
import { ProductSoldComponent } from './product-sold/product-sold.component';
import { TeamMemberComponent } from './team-member/team-member.component';
import { OrderListComponent } from './order-list/order-list.component';
import { ProdcutAddComponent } from './prodcut-add/prodcut-add.component';
import { LogindetailsComponent } from './logindetails/logindetails.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { CompanyAddComponent } from './company-add/company-add.component';
import { ProductChartComponent } from './product-chart/product-chart.component';
//import { RegisterFormComponent } from './register-form/register-form.component';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddressComponent } from './address/address.component';
import { SlideBarComponent } from './slide-bar/slide-bar.component';
import { HeaderAreaComponent } from './header-area/header-area.component';
import { OffsetAreaComponent } from './offset-area/offset-area.component';
import { InvoiceComponent } from './invoice/invoice.component';
import {MatDialogModule} from '@angular/material/dialog';
import { OrderComponent } from './order/order.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { OrdersComponent } from './orders/orders.component';
import { ToastrModule } from 'ngx-toastr';
import { OrderService } from './services/order.service';
import { FeedbackFormComponent } from './feedback-form/feedback-form.component';
import { SeoDashboardComponent } from './seo-dashboard/seo-dashboard.component';
import { ArchwizardModule } from 'angular-archwizard';
import { CampignComponent } from './campign/campign.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { ShowItemsForSingleOrderComponent } from './show-items-for-single-order/show-items-for-single-order.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { CustomerComponent } from './customer/customer.component';
import { CustomersComponent } from './customers/customers.component';
import { ReputationsComponent } from './reputations/reputations.component';
import { FacebookpostComponent } from './services/facebookpost/facebookpost.component';
import { ReportComponent } from './report/report.component';
import { TodaysOfferReportComponent } from './services/todays-offer-report/todays-offer-report.component';
import { StocksComponent } from './services/stocks/stocks.component';
import { StockComponent } from './services/stock/stock.component';
import { MemberComponent } from './member/member.component'; 
import { ClientFeedbackComponent } from './client-feedback/client-feedback.component';
import {MatDatepickerModule} from '@angular/material';
import { MatNativeDateModule } from '@angular/material/core';
import { CustomerComponentPopUp } from './customer/popup/customer.component';
import { SearchByFieldsPipe } from './search-by-fields.pipe';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { StockSearchPipe } from './stock-search.pipe';
import { CustomerSearchPipe } from './customer-search.pipe';
import { VendorComponent } from './vendor/vendor.component';
import { VendorsComponent } from './vendors/vendors.component';
import { AuditlogComponent } from './auditlog/auditlog.component';
import { AuditlogPipe } from './auditlog.pipe';
declare var require:any;

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    routingComponents,
   HomeComponent,
   PageTitleComponent,
   ReportAreaComponent,
   ProductSoldComponent,
   TeamMemberComponent,
   OrderListComponent,
   ProdcutAddComponent,
   LogindetailsComponent,
   ShowProductsComponent,
   CompanyAddComponent,
   ProductChartComponent,
   AddressComponent,
   SlideBarComponent,
   HeaderAreaComponent,
   OffsetAreaComponent,
   InvoiceComponent,
   OrderComponent,
   OrderItemComponent,
   OrdersComponent,
   FeedbackFormComponent,
   SeoDashboardComponent,
   CampignComponent,
   CampaignsComponent,
   ShowItemsForSingleOrderComponent,
   CustomerComponent,
   CustomerComponentPopUp,
   CustomersComponent,
   ReputationsComponent,
   FacebookpostComponent,
   ReportComponent,
   TodaysOfferReportComponent,
   StocksComponent,
   StockComponent,
   MemberComponent,
   ClientFeedbackComponent,
   SearchByFieldsPipe,
   InvoiceListComponent,
   StockSearchPipe,
   CustomerSearchPipe,
   VendorComponent,
   VendorsComponent,
   AuditlogComponent,
   AuditlogPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    ArchwizardModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    NgxPaginationModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents:[OrderItemComponent,ShowItemsForSingleOrderComponent,CustomerComponent,CustomerComponentPopUp,MemberComponent],
  providers: [OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


