import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-seo-dashboard',
  templateUrl: './seo-dashboard.component.html',
  styleUrls: ['./seo-dashboard.component.css']

})
export class SeoDashboardComponent implements OnInit {

  constructor() {
    this.loadScripts();
  }

  ngOnInit() {
  }

  loadScripts() {
    const dynamicScripts = [
      '../../assets/js/line-chart.js',
      '../../assets/js/pie-chart.js',
      '../../assets/js/bar-chart.js',
      '../../assets/js/maps.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
}

