import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { OrderService } from '../services/order.service';
import { ActivatedRoute, Router } from '@angular/router';
import JsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  todaysDate: Date = new Date();
  constructor(private service: OrderService, private router: Router,
    private currentRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    let orderID = this.currentRoute.snapshot.paramMap.get('id');
    if (orderID) {
      this.service.getOrderByID(parseInt(orderID)).then(res => {
        this.service.formData = res;
        this.service.orderItems = res.orderItems;
        console.log(res)
      });
    }
  }

  navigateToPage(orderType) {
    if(orderType===0) {
      this.router.navigate(['home/orders/']);
    }else {
      this.router.navigate(['home/stocks/']);
    }
  }

  downloadInvoice (customerName) {
    var data=document.getElementById("invoiceDownload");
    console.log(data);
    html2canvas(data,{scrollY: -window.scrollY, 
      scale: 1}).then( canvas =>{
        var imgWidth = 208;
        var imgHeight = canvas.height * imgWidth / canvas.width;
      var contentDataUrl=canvas.toDataURL("image/png");
      var pdf = new JsPDF(); // A4 size page of PDF
      pdf.addImage(contentDataUrl, 0, 0, imgWidth, imgHeight)
      pdf.save(customerName+'.pdf'); // Generated PDF
    });
  }

}
