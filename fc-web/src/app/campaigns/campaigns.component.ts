import { Component, OnInit } from '@angular/core';
import { Campaign } from '../beans/Campaign';
import { CampaignServiceService } from '../services/campaign-service.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.css']
})
export class CampaignsComponent implements OnInit {

  campaignsList: Campaign[];

  constructor(private service: CampaignServiceService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {

    this.service.getAllCampaigns().then(res => {
    this.campaignsList = res as Campaign[]
      console.log(res)
    });
  }


  onCampaignDelete(id: number) {
    if (confirm('Are you sure to delete this record?')) {
      this.service.deleteCampaign(id).then(res => {
        this.refreshList();
        this.toastr.warning("Deleted Successfully", "Restaurent App.");
      });
    }
  }
  onCampaignLunch(id: number,name:String) {
    if (confirm('Do you want to lunch '+name+ ' Campaign ?')) {
      this.service.lunchCampaign(id).then(res => {
        this.refreshList();
        this.toastr.warning("Lunched Successfully", "Campaign.");
      });
    }
  }
  onCampaignEdit(id: number,name:String) {
    if (confirm('Do you want to Edit '+name+ ' Campaign?')) {
      this.router.navigate(['home/campaign/edit/' + id]);
    }
  }
}
