import { Company } from "./Company";

export class Product {

    public id: number;
    public name: string;
    public quantity: number;
    public buyPrice: number;
    public sellPrice: number;
    public uniqueNumber: String;
    public expiredate: number;
    public stateGST:number;
    public govtGST:number;
    public unitId:number;
    public stockAlert:boolean;
    public description:string;
    public vendorId:number;
    constructor() { }

}