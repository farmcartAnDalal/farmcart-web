export class Vendor {
    id: number;
    name: string;
    contactNumber: number
    emailId: number
    address: string
    isActive:number
    dateOfBirth:Date
    type:string
    panCardNumber:string
    bankAccountNumber:string
    bankAccountName:string
    ifscCode:string
}
