import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.component.html',
  styleUrls: ['./feedback-form.component.css']
})
export class FeedbackFormComponent implements OnInit {

  feedbackForm : FormGroup;


  constructor(public feedbackService:CustomerService,private formBuilder : FormBuilder) { 
    
  }

  //Creating form
  public formCreation()
  {
    this.feedbackForm = this.formBuilder.group({
      name:[],
      emailId:[],
      subject:[],
      message:[],
      mobileNumber:0
    });
      
  }
  ngOnInit() {
    this.formCreation();
  }


  onSubmit(){
    console.log(this.feedbackForm.value);
    this.feedbackService.storeFeedback(this.feedbackForm.value)
  }
  

}
