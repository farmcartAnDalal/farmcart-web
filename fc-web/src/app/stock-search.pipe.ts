import { Pipe, PipeTransform } from '@angular/core';
import { Order } from './beans/order';

@Pipe({
  name: 'stockSearch'
})
export class StockSearchPipe implements PipeTransform {

  transform(orders: Order[], searchText: string): any {
    if (orders) {
      return orders.filter(order => { return (order.customerName.indexOf(searchText) != -1) });
    }
  }

}
