import { Component, OnInit } from '@angular/core';
import { Customer } from '../beans/customer';
import { CustomerService } from '../services/customer.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers:Customer[];
  searchText = '';
  constructor(private customerService :CustomerService,private router: Router, private toastr: ToastrService) { }


  ngOnInit() {
    this.customerService.getCustomers().then(res=>{
      this.customers=res;
    });
  }
  onCustomerDelete(id:number)
  {
    if (confirm('Are you sure to delete this record?')) {
      this.customerService.deleteCustomer(id).then(res => {
        this.toastr.warning("Deleted Successfully", "Customer");
        this.router.navigate(['home/customers']);
      });
    }
  }
  onCustomerEdit(id:number,name:String)
  {
    if (confirm('Are you sure to edit this record?')) {
      this.router.navigate(['home/customer/edit/' + id]);
    }
  }
}
