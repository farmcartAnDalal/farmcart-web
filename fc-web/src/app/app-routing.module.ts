import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterFormComponent } from './register-form/register-form.component'
import { LoginFormComponent } from './login-form/login-form.component';
import { HomeComponent } from './home/home.component';
import { ProdcutAddComponent } from './prodcut-add/prodcut-add.component';
import { LogindetailsComponent } from './logindetails/logindetails.component';
import { ShowProductsComponent } from './show-products/show-products.component';
import { CompanyAddComponent } from './company-add/company-add.component';
import { ProductChartComponent } from './product-chart/product-chart.component';
import { ReportAreaComponent } from './report-area/report-area.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderComponent } from './order/order.component';
import { OrdersComponent } from './orders/orders.component';

import { FeedbackFormComponent } from './feedback-form/feedback-form.component';
import { SeoDashboardComponent } from './seo-dashboard/seo-dashboard.component';
import { CampignComponent } from './campign/campign.component';
import { CampaignsComponent } from './campaigns/campaigns.component';\
import { CustomersComponent } from './customers/customers.component';
import { CustomerComponent } from './customer/customer.component';
import { ReputationsComponent } from './reputations/reputations.component';
import { FacebookpostComponent } from './services/facebookpost/facebookpost.component';
import { ReportComponent } from './report/report.component';
import { TodaysOfferReportComponent } from './services/todays-offer-report/todays-offer-report.component';
import { StocksComponent } from './services/stocks/stocks.component';
import { StockComponent } from './services/stock/stock.component';
import { AuthGuard, JwtInterceptor } from './helper';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { VendorComponent } from './vendor/vendor.component';
import { VendorsComponent } from './vendors/vendors.component';
import { AuditlogComponent } from './auditlog/auditlog.component';


const routes: Routes = [
  { path: 'register', component: RegisterFormComponent },
  { path: 'login', component: LoginFormComponent },
  { path: 'feedbackForm', component: FeedbackFormComponent },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'reportHome', component: ReportAreaComponent },
      { path: 'showProducts', component: ShowProductsComponent },
      { path: 'productAdd', component: ProdcutAddComponent },
      { path: 'productAdd/edit/:id', component: ProdcutAddComponent },
      { path: 'feedbackForm', component: FeedbackFormComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'order', component: OrderComponent },
      { path: "order/invoice/:id", component: InvoiceComponent },
      { path: 'order/edit/:id', component: OrderComponent },
      { path: 'seoDashboard', component: SeoDashboardComponent },
      { path: 'campaign', component: CampignComponent },
      { path: 'campaign/edit/:id', component: CampignComponent },
      { path: 'campaigns', component: CampaignsComponent },
      { path: 'customers', component: CustomersComponent },
      { path: 'customer', component: CustomerComponent },
      { path: 'customer/edit/:id', component: CustomerComponent },
      { path: 'facebookpost/edit/:id', component: FacebookpostComponent },
      { path: 'reputations', component: ReputationsComponent },
      { path: 'invoices', component: InvoiceListComponent },
      { path: 'stocks', component: StocksComponent },
      { path: 'stock', component: StockComponent },
      { path: 'stock/edit/:id', component: StockComponent },
      { path: 'auditlog', component: AuditlogComponent },
      { path: 'vendor', component: VendorComponent },
      { path: 'vendors', component: VendorsComponent },
      { path: 'vendor/edit/:id', component: VendorComponent },
      {
        path: 'report', component: ReportComponent,
        children: [
          { path: 'todaysorderreport', component: TodaysOfferReportComponent }
        ]
      }


    ]
  },
  { path: 'campaign', component: CampignComponent },
  { path: 'ceo', component: LogindetailsComponent },
  { path: 'companyadd', component: CompanyAddComponent },
  { path: 'productchart', component: ProductChartComponent },
  { path: 'invoice', component: InvoiceComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },

  { path: 'orders', component: OrdersComponent },
  {
    path: 'order', children: [
      { path: '', component: OrderComponent },
      { path: 'edit/:id', component: OrderComponent }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },

   
],
})
export class AppRoutingModule { }
export const routingComponents = [RegisterFormComponent]