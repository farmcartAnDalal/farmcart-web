import { Component, OnInit } from '@angular/core';
import { Order } from '../beans/order';
import { OrderService } from '../services/order.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OrderItemComponent } from '../order-item/order-item.component';
import { ShowItemsForSingleOrderComponent } from '../show-items-for-single-order/show-items-for-single-order.component';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  todaysOrdersList:Order[];
  orderNo : Number;
  constructor(private service: OrderService,private dialog: MatDialog) { }

  ngOnInit()  {
    this.refreshList();
  }

  refreshList() {
    this.service.getOrderList(parseInt("0")).then(res => this.todaysOrdersList = res as Order[]);
  }

  displayTodaysOrderList(orderId:number)
  {
    console.log("goot id::"+orderId)
   this.service.getItemListByOrderID(orderId).then(res => {console.log(res)
  
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
    dialogConfig.data = { res};
    this.dialog.open(ShowItemsForSingleOrderComponent, dialogConfig).afterClosed().subscribe(res => {
     
    });
  
  });
   
  }

}
