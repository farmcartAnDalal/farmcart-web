import { Component, OnInit } from '@angular/core';
import { VendorService } from '../services/VendorService';
import { Vendor } from '../beans/Vendor';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.css']
})
export class VendorsComponent implements OnInit {

  vendors:Vendor[];
  searchText = '';
  constructor(private VendorService :VendorService,private router: Router, private toastr: ToastrService) { }


  ngOnInit() {
    this.VendorService.getVendors().then(res=>{
      this.vendors=res;
    });
  }
  onVendorDelete(id:number)
  {
    if (confirm('Are you sure to delete this record?')) {
      this.VendorService.deleteVendor(id).then(res => {
        this.toastr.warning("Deleted Successfully", "Vendor");
        this.router.navigate(['home/vendors']);
      });
    }
  }
  onVendorEdit(id:number,name:String)
  {
    if (confirm('Are you sure to edit this record?')) {
      this.router.navigate(['home/vendor/edit/' + id]);
    }
  }

}
