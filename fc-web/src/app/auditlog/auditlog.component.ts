import { Component, OnInit } from '@angular/core';
import { AuditLogService } from '../audit-log.service';
import { AuditLog } from '../beans/AuditLog';

@Component({
  selector: 'app-auditlog',
  templateUrl: './auditlog.component.html',
  styleUrls: ['./auditlog.component.css']
})
export class AuditlogComponent implements OnInit {

  auditLogs:AuditLog[];
  searchText='';
  constructor( private auditLogService:AuditLogService) { }

  
  ngOnInit() {
    //this.prods=this.products.getAll() ;
    this.auditLogService.getAll().subscribe(res => {
      this.auditLogs = res;
   });
  }
  refreshList()
  {
    this.auditLogService.getAll().subscribe(res => {
      this.auditLogs = res;
   });
  }

}
