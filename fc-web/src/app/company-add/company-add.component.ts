import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl ,FormBuilder, Validators} from '@angular/forms';


@Component({
  selector: 'app-company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.css']
})
export class CompanyAddComponent implements OnInit {

  constructor(private fb:FormBuilder) { }

  companyForm=this.fb.group({
    nameControl:["Dadasaheb boss",Validators.required],
    regNumberControl:["DOPPK9575E"],
    address:this.fb.group(
     {addressControl:["A/P -Tadwale(Ya)"]}
    )
  });

  companyForm1 = new FormGroup(
    {
      nameControl: new FormControl("Samsung"),
      regNumberControl: new FormControl("DOPPK9575E"),
      address: new FormGroup(
        { 
          addressControl: new FormControl("A/P-tadwale(ya)") 
      })
    });

  ngOnInit() {
  }

  cForm()
  {
    return "companyForm.get('nameControl')";
  }
  loadDataFromSetvalue()
  {
    this.companyForm.setValue({
      nameControl: new FormControl("Samsung"),
      regNumberControl: new FormControl("DOPPK9575E"),
      address: new FormGroup(
        { 
          addressControl: new FormControl("A/P-tadwale(ya) from setValue") 
      })
    });
  }
  loadDataFromPatch()
  {
    console.log("Patch Value Methode called");
    this.companyForm.patchValue({
      nameControl: new FormControl("Dadasaheb"),
      regNumberControl: new FormControl("DOPPK9575E"),
      address: new FormGroup(
        { 
          addressControl: new FormControl("A/P-tadwale(ya) from patch") 
      })
    });
  }
}
