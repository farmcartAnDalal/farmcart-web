import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { FeedbackFormComponent } from '../feedback-form/feedback-form.component';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-client-feedback',
  templateUrl: './client-feedback.component.html',
  styleUrls: ['./client-feedback.component.css']
})
export class ClientFeedbackComponent implements OnInit {

  constructor(private dialog: MatDialog,private customerService:CustomerService) { }

  feedBacks :any[];

  ngOnInit() {
    this.customerService.getFeedBacks().then(res=>{
      this.feedBacks=res;
    })
  }

  createFeedBack()
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
    this.dialog.open(FeedbackFormComponent, dialogConfig).afterClosed().subscribe(res => {
      
    });
  }
}
