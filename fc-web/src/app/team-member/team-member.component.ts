import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { MemberComponent } from '../member/member.component';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-team-member',
  templateUrl: './team-member.component.html',
  styleUrls: ['./team-member.component.css']
})
export class TeamMemberComponent implements OnInit {

  constructor(private dialog: MatDialog,
              private reportService:ReportService) { }

  members :any[];

  ngOnInit() {
    this.getMembers();
  }

  getMembers()
  {
    this.reportService.getMembers().then(res=>{
      this.members=res;
    })
  }
  createMember()
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "50%";
    this.dialog.open(MemberComponent, dialogConfig).afterClosed().subscribe(res => {
      
    });
  }
}
