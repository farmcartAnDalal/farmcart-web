import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAreaComponent } from './report-area.component';

describe('ReportAreaComponent', () => {
  let component: ReportAreaComponent;
  let fixture: ComponentFixture<ReportAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
