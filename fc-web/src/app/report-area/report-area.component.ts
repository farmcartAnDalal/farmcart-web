import { Component, OnInit } from '@angular/core';
import { Order } from '../beans/order';
import { OrderService } from '../services/order.service';
import * as Highcharts from 'highcharts';
import { ReportService } from '../services/report.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-report-area',
  templateUrl: './report-area.component.html',
  styleUrls: ['./report-area.component.css']
})
export class ReportAreaComponent implements OnInit {
  cashFLow: number[]=new Array();
  private headers = new Headers({ 'Access-Control-Allow-Origin': 'http://127.0.0.1:4200' });
  constructor( private reportService: ReportService) {
  }


  ngOnInit() {
  }

  
}

