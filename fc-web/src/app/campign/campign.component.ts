import { Component, OnInit } from '@angular/core';
import { Campaign } from '../beans/Campaign';
import { CampaignServiceService } from '../services/campaign-service.service';
import { CampaignRule } from '../beans/CampaignRule';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-campign',
  templateUrl: './campign.component.html',
  styleUrls: ['./campign.component.css']
})
export class CampignComponent implements OnInit {

  campaign: Campaign;
  cName: String ="Campaign";
  saveSuccess: boolean = false;
  campaignRule: CampaignRule[];
  type: string = "number";
  constructor(private campaignRuleService: CampaignServiceService,
    private router: Router, private toastr: ToastrService, private currentRoute: ActivatedRoute) {

  }

  ngOnInit() {
    let campaignId = this.currentRoute.snapshot.paramMap.get('id');
    this.campaignRuleService.getAllCampaignRule().then(res => {
      this.campaignRule = res;
      if (campaignId == null) {
        this.campaign = {
          id: null,
          name: this.cName,
          ruleNumber: 0,
          value: "",
          message: ""
        };
      } else {
        this.campaignRuleService.getCampaignById(parseInt(campaignId)).then(res => {
          this.campaign = res;
        }
        )
      }
    })
    this.changeInputType(null);
  }

  changeInputType(ctrl) {
    if(ctrl===null)
    {
      this.type="text";
    }else if(this.campaign.ruleNumber==9 || this.campaign.ruleNumber==10 )
    {
      this.type="text";
    }else{
      this.type="number";
    }
  }

  finishFunction() {
    this.campaignRuleService.saveCampaign(this.campaign).then(res => {
      this.saveSuccess = res;
      if (this.saveSuccess) {
        console.log(this.saveSuccess);
        this.router.navigate(['/home/campaigns']);
        this.toastr.success('Submitted Successfully', 'Campaign');
      }
      else {
        this.toastr.success('Please check campaign value');
      }
    })

  }



}
