import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampignComponent } from './campign.component';

describe('CampignComponent', () => {
  let component: CampignComponent;
  let fixture: ComponentFixture<CampignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
