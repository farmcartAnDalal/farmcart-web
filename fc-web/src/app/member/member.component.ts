import { Component, OnInit, Inject } from '@angular/core';
import { ReportService } from '../services/report.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  member:any;
  constructor( private reportServce:ReportService,
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<MemberComponent>,) { }
  ngOnInit() {
    this.member={
      name:null,
      emailId:null,
      contactNumber:null,
      designation:null
    }
  }

  saveMember()
  {
    this.reportServce.saveMember(this.member).then(res=>{
      this.dialogRef.close();
    })
  }
}
