import { Pipe, PipeTransform } from '@angular/core';
import { AuditLog } from './beans/AuditLog';

@Pipe({
  name: 'auditlog'
})
export class AuditlogPipe implements PipeTransform {

  transform(customers: AuditLog[], searchText: string): any {
    if(customers) {
      return customers.filter(customer=>{ return customer.entity.indexOf(searchText)!=-1 || customer.entityName.indexOf(searchText)!=-1});
    }
   }

}
