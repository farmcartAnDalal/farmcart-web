import { Component, OnInit } from '@angular/core';
import { SocialMediaType } from '../beans/SocialMediaType';
import { ReputationService } from '../services/reputation.service';
import { Post } from '../beans/Post';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reputations',
  templateUrl: './reputations.component.html',
  styleUrls: ['./reputations.component.css']
})
export class ReputationsComponent implements OnInit {

  selectedSocialId :number
  socialMediaList:SocialMediaType[]=[{id:1,name:"Facebook"}]
  constructor(private reputationService:ReputationService, private router: Router, private toastr: ToastrService) { }
  
  postList:Post[];

  ngOnInit() {
    this.selectedSocialId=1;
    this.connectFacebook();
  }

  changeItemStatus(event:any)
  {
    console.log(this.selectedSocialId);
  }

  connectFacebook()
  {
    console.log('called from ui');
    this.reputationService.connectFacebook().then(ele=>{
      console.log("facebookconnected");
    });
    this.getFacebookPost();
  }
  getFacebookPost()
  {
    this.reputationService.getFacebookPost().then(res=>{
      this.postList=res;
    });
  }
  onPostView(id:String,name:String)
  {
    if (confirm('Do you want to see post stats')) {
      this.router.navigate(['home/facebookpost/edit/' + id]);
    }
  }

}
