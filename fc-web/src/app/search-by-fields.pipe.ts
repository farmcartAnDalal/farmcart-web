import { Pipe, PipeTransform } from '@angular/core';
import { Product } from './beans/Product';

@Pipe({
  name: 'searchByFields'
})
export class SearchByFieldsPipe implements PipeTransform {

  transform(products: Product[], searchText: string): any {
    if (products) {
      return products.filter(product => { return product.name.indexOf(searchText) != -1 });
    }
  }
}
