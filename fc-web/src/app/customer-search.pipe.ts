import { Pipe, PipeTransform } from '@angular/core';
import { Customer } from './beans/customer';

@Pipe({
  name: 'customerSearch'
})
export class CustomerSearchPipe implements PipeTransform {

  transform(customers: Customer[], searchText: string): any {
    if (customers) {
      return customers.filter(customer => { return customer.name.indexOf(searchText) != -1 });
    }
  }

}
