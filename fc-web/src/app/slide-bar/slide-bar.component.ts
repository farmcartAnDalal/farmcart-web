import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slide-bar',
  templateUrl: './slide-bar.component.html',
  styleUrls: ['./slide-bar.component.css']
})
export class SlideBarComponent implements OnInit {

  constructor() { }
  colButton = false;
  colroduct = false;
  colDashboard=false;
  colCamp=false;
  colCustomer=false;
  colRepu=false;
  colRepo=false;
  colStock=false;
  colInvoices=false;
  colSell=false;
  colVendors=false;
  colAudit=false;
  ngOnInit() {
  }

  collaseButton() {

    this.colButton = !this.colButton;
  }

  colButtonSell () {
    this.colSell = !this.colSell;
  }

  colButtonProduct() {

    this.colroduct = !this.colroduct;
  }

  colButtonDashboard()
  {
    this.colDashboard = !this.colDashboard;
  }
  collaseCampButton()
  {
    this.colCamp=!this.colCamp;
  }
  collaseCustomerButton()
  {
    this.colCustomer=!this.colCustomer;
  }
  collaseRepuButton()
  {
    this.colRepu= !this.colRepu;
  }
  collaseRepoButton()
  {
    this.colRepo=!this.colRepo;
  }

  collaseStockButton()
  {
    this.colStock=!this.colStock;
  }

  collaseInvoiceButton() {
    this.colInvoices=!this.colInvoices;
  }

  collaseVendorsButton() {
    this.colVendors=!this.colVendors;
  }
  collaseAuditlogButton(){
    this.colAudit=!this.colAudit;
  }
}
