import { Component, OnInit } from '@angular/core';
import { Order } from '../beans/order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-product-sold',
  templateUrl: './product-sold.component.html',
  styleUrls: ['./product-sold.component.css']
})
export class ProductSoldComponent implements OnInit {

  todaysOrdersList:Order[];
  constructor(private service: OrderService) { }
  
  ngOnInit()  {
    this.refreshList();
  }

  refreshList() {
    this.service.getOrderList(parseInt("0")).then(res => this.todaysOrdersList = res as Order[]);
  }
}
