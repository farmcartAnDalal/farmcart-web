import { Component, OnInit } from '@angular/core';
import { Address } from '../beans/Address';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  addressForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }
  public address = new Address();
  ngOnInit() {
    this.createUserForm();
  }

  createUserForm() {
    this.addressForm = this.formBuilder.group({
      addressLine1: [],
      addressLine2: [],
      city: [],
      state: [],
      country: [],
      pinCode: []
    });


  }

}
