export const environment = {
  production: true,
  apiURL : 'http://localhost:8080/',
  feedbackServiceURL : 'http://localhost:8080/feedbackForm',
  faceBookServerURL:'http://localhost:8080/'
};
